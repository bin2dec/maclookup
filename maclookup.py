#!/usr/bin/env python

from argparse import ArgumentParser, FileType

import sys
import os

DEFAULT_OUI_LIST = os.path.join(sys.path[0], 'oui.txt')


def oui_type(value):
    value = value.upper()
    value = ''.join(i for i in value if i in '0123456789ABCDEF')
    if len(value) < 6:
        raise ValueError()
    value = value[:6]
    return value


if __name__ == '__main__':
    argparser = ArgumentParser(description='it is a tool to look up an organization name by OUI or MAC address')

    argparser.add_argument(
        'oui',
        help='OUI or MAC address',
        nargs='+',
        type=oui_type,
    )

    argparser.add_argument(
        '--oui-list',
        default=DEFAULT_OUI_LIST,
        help='IEEE OUI list',
        metavar='file',
        type=FileType(),
    )

    args = argparser.parse_args()

    oui_list = args.oui[:]
    out = []

    for ln in args.oui_list:
        ln = ln.lstrip()
        oui = ln[:6]
        if oui in oui_list:
            out.append('{}  {}'.format(oui, ' '.join(ln.split()[3:])))
            if len(oui_list) == 1:
                break
            oui_list.remove(oui)

    out.sort(key=lambda x: args.oui.index(x[:6]))
    print('\n'.join(out))
