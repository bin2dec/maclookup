maclookup
=========

    usage: maclookup.py [-h] [--oui-list file] oui [oui ...]

    it is a tool to look up an organization name by OUI or MAC address

    positional arguments:
      oui              OUI or MAC address

    optional arguments:
      -h, --help       show this help message and exit
      --oui-list file  IEEE OUI list
